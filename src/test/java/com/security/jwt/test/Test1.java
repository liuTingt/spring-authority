package com.security.jwt.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.security.jwt.services.RbacAuthorityService;
import com.security.jwt.util.SpringUtils;

public class Test1 {
	
	public static void main(String[] args) {
		Map<String, Object> map = new HashMap<>();
		
		map.put("first", "first Element");
		map.put("second", "second Element");
		map.put("third", "third Element");
		map.put("four", "four Element");
		
		for (Map.Entry<String, Object> set : map.entrySet()) {
			System.out.println(set.getKey()+"--"+set.getValue());
			System.out.println(map.get(set.getKey()));
		}
		
		RbacAuthorityService rbacAuthorityService = (RbacAuthorityService) SpringUtils.getBean("rbacauthorityservice");
		System.out.println(rbacAuthorityService);
	}
}
