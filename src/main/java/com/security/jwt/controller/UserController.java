package com.security.jwt.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.security.jwt.service.IUserService;
import com.security.jwt.util.ResultUtil;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private IUserService userService;
	
	@GetMapping("/list")
	public ResultUtil<?> list() {
		return userService.list();
	}
	
	@GetMapping("/logout")
	public ResultUtil<?> login(HttpServletRequest request, HttpServletResponse response) {
		return userService.logout(request, response);
	}
	
	@GetMapping("/hello")
	public ResultUtil<?> hello() {
		return ResultUtil.success("不需要权限也可以访问！");
	}
	
}

