package com.security.jwt.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

}

