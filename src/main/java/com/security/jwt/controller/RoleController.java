package com.security.jwt.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.security.jwt.service.IRoleService;
import com.security.jwt.util.ResultUtil;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private IRoleService roleService;
	
	@SuppressWarnings("rawtypes")
	public ResultUtil list(){
		return roleService.list();
	}
}

