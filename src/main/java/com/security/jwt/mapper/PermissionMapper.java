package com.security.jwt.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.security.jwt.dto.PermissionDTO;
import com.security.jwt.entity.Permission;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Repository
public interface PermissionMapper extends BaseMapper<Permission> {

	List<PermissionDTO> findAll();
}
