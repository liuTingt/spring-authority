package com.security.jwt.mapper;

import com.security.jwt.entity.Role;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Repository
public interface RoleMapper extends BaseMapper<Role> {

}
