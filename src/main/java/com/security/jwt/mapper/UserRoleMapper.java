package com.security.jwt.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.security.jwt.entity.Role;
import com.security.jwt.entity.UserRole;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Repository
public interface UserRoleMapper extends BaseMapper<UserRole> {

	/**
	 * 
	 * @Description 
	 *	获取用户角色
	 * @param userId
	 * @return
	 */
	List<Role> roles(String userId);
}
