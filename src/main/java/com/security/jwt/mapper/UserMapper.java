package com.security.jwt.mapper;

import org.springframework.stereotype.Repository;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.security.jwt.dto.UserDTO;
import com.security.jwt.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

	UserDTO getUserDTO(String phone);
}
