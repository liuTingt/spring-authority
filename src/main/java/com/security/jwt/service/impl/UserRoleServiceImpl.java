package com.security.jwt.service.impl;

import com.security.jwt.entity.UserRole;
import com.security.jwt.mapper.UserRoleMapper;
import com.security.jwt.service.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

}
