package com.security.jwt.service.impl;

import com.security.jwt.entity.RolePermission;
import com.security.jwt.mapper.RolePermissionMapper;
import com.security.jwt.service.IRolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements IRolePermissionService {

}
