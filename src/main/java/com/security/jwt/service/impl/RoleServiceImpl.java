package com.security.jwt.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.security.jwt.entity.Role;
import com.security.jwt.mapper.RoleMapper;
import com.security.jwt.service.IRoleService;
import com.security.jwt.util.ResultUtil;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

	@SuppressWarnings("unchecked")
	@Override
	public ResultUtil<List<Role>> list() {
		List<Role> list = this.list(new QueryWrapper<Role>().lambda()
				.eq(Role::getRoleStatus, '0'));
		return ResultUtil.success(list);
	}

	

}
