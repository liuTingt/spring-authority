package com.security.jwt.service.impl;

import com.security.jwt.entity.Permission;
import com.security.jwt.mapper.PermissionMapper;
import com.security.jwt.service.IPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {

}
