package com.security.jwt.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.security.jwt.dto.PermissionMap;
import com.security.jwt.entity.User;
import com.security.jwt.mapper.UserMapper;
import com.security.jwt.service.IUserService;
import com.security.jwt.util.ResultUtil;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

	@SuppressWarnings("unchecked")
	@Override
	public ResultUtil<List<User>> list() {
		List<User> list = this.list(new QueryWrapper<User>().lambda()
				.eq(User::getUserStatus, '0'));
		return ResultUtil.success(list);
	}

	@Override
	public ResultUtil<?> logout(HttpServletRequest request, HttpServletResponse response) {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication != null) {
			new SecurityContextLogoutHandler().logout(request, response, authentication);
			PermissionMap.map = null;
			PermissionMap.list = null;
		}
		
		return ResultUtil.success();
	}

	
}
