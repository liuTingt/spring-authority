package com.security.jwt.service;

import com.security.jwt.entity.RolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
public interface IRolePermissionService extends IService<RolePermission> {

}
