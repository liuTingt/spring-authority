package com.security.jwt.service;

import com.security.jwt.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
public interface IUserRoleService extends IService<UserRole> {

}
