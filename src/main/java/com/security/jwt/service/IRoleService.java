package com.security.jwt.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.jwt.entity.Role;
import com.security.jwt.util.ResultUtil;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
public interface IRoleService extends IService<Role> {

	ResultUtil<List<Role>> list();
}
