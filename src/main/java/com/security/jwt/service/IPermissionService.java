package com.security.jwt.service;

import com.security.jwt.entity.Permission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
public interface IPermissionService extends IService<Permission> {

}
