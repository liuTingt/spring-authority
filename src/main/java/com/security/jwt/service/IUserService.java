package com.security.jwt.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.extension.service.IService;
import com.security.jwt.entity.User;
import com.security.jwt.util.ResultUtil;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ltt
 * @since 2019-05-31
 */
public interface IUserService extends IService<User> {

	ResultUtil<List<User>> list();
	
	ResultUtil<?> logout(HttpServletRequest request, HttpServletResponse response);
}
