package com.security.jwt.enums;

import lombok.Getter;
/**
 * 
 * @Description 
 *	错误枚举类
 * @author lt
 *
 */
@Getter
public enum ResultEnum {

	ACCESS_NOT(501, "权限不足"),
	
	TOKEN_IS_NOT_VALID(502, "token无效，请重新登陆");
	
	private Integer code;
	
	private String msg;
	
	ResultEnum(Integer code, String msg){
		this.code = code;
		this.msg = msg;
	}
}
