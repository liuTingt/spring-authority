package com.security.jwt.util;

/**
 * 
 * @Description 
 *	时间工具类
 * @author lt
 *
 */
public class TimeUtil {

	/**
	 * 
	 * @Description 
	 *	当前时间戳
	 * @return
	 */
	public static String nowTimeStamp() {
		return String.valueOf(System.currentTimeMillis());
	}
}
