package com.security.jwt.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.security.jwt.dto.UserDTO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;

/**
 * 
 * @Description 
 *	JWT token生成
 * @author lt
 *
 */
@Component
@ConfigurationProperties(prefix = "jwt")
@Data
public class JwtTokenUtil {

	
	private String tokenHeader;
	
	/**
	 * token前缀
	 */
	private String tokenPrefix;
	
	/**
	 * 密钥
	 */
	private String secret;
	
	 /**
     * 过期时间
     */
	private Long expiration;
	
	/**
     * 选择记住后过期时间
     */
	private Long rememberExpiration;
	
	
	private static final String CLAIM_KEY_AUTHORITIES = "user_scope";
	
	
	/**
	 * 
	 * @Description 
	 *	生成token
	 * @return
	 */
	public String generationToken(UserDTO userDTO) {
		Long time = userDTO.getRemember() ? this.rememberExpiration : this.expiration;
		Map<String, Object> map = new HashMap<>(1);
		map.put(CLAIM_KEY_AUTHORITIES, userDTO);
		return Jwts.builder()
				.setClaims(map)
				.setSubject(userDTO.getUsername())
				.setExpiration(new Date(System.currentTimeMillis() + time * 1000))
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}

	/**
	 * 
	 * @Description 
	 *	获取用户名
	 * @param token
	 * @return
	 */
	public String getUserNameFromToken(String token) {
		return getClaimsFormToken(token).getSubject();
	}
	
	/**
	 * 
	 * @Description 
	 *	获取Claims
	 * @param token
	 * @return
	 */
	public Claims getClaimsFormToken(String token) {
		return Jwts.parser()
				.setSigningKey(secret)
				.parseClaimsJws(token)
				.getBody();
	}
	
	/**
	 * 
	 * @Description 
	 *	获取User
	 * @param token
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public UserDTO getUserDTO(String token) {
		Claims claims = getClaimsFormToken(token);
		Map<String, String> map = claims.get(CLAIM_KEY_AUTHORITIES, Map.class);
		UserDTO userDTO = JSON.parseObject(JSON.toJSONString(map), UserDTO.class);
		return userDTO;
	}
}
