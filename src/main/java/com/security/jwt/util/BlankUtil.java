package com.security.jwt.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/***
 * 
 * @Description 
 *	校验是否为空
 * @author lt
 *
 */
public class BlankUtil {

	
	/**
	 * 防止实例化
	 */
	private BlankUtil() {
		
	}

	/**
	 * 
	 * @Description 
	 *	判断字符串是否为空（包括null， 空字符串）
	 * @param str
	 * @return
	 */
	public static boolean isBlank(final String str) {
		return (str == null) || (str.trim().length() <= 0);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断字符是否为空
	 * @param ch
	 * @return
	 */
	public static boolean isBlank(final Character ch) {
		return (ch == null) || (ch.equals(' '));
	}
	
	/**
	 * 
	 * @Description 
	 *	判断对象是否为空
	 * @param obj
	 * @return
	 */
	public static boolean isBlank(final Object obj) {
		return (obj == null);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断数组是否为空
	 * @param objs
	 * @return
	 */
	public static boolean isBlank(final Object[] objs) {
		return (objs == null) || (objs.length <= 0);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断集合Colleation是否为空
	 * @param coll
	 * @return
	 */
	public static boolean isBlank(final Collection<?> coll) {
		return (coll == null);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断set是否为空
	 * @param set
	 * @return
	 */
	public static boolean isBlank(final Set<?> set) {
		return (set == null);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断整数是否为空或小于0
	 * @param i
	 * @return
	 */
	public static boolean isBlank(final Integer i) {
		return (i == null) || (i < 1);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断serializable是否为空
	 * @param seri
	 * @return
	 */
	public static boolean isBlank(final Serializable seri) {
		return seri == null;
	}
	
	/**
	 * 
	 * @Description 
	 *	判断map是否为空
	 * @param map
	 * @return
	 */
	public static boolean isBlank(final Map<?, ?> map) {
		return (map == null) || (map.size() <= 0);
	}
	
	/**
	 * 
	 * @Description 
	 *	判断list是否为空
	 * @param list
	 * @return
	 */
	public static boolean isBlank(final List<?> list) {
		return (list == null) || (list.size() <= 0);
	}
}
