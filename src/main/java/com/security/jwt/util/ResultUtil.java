package com.security.jwt.util;
/**
 * 
 * @Description 
 *	结果类
 * @author lt
 *
 */

import com.security.jwt.enums.ResultEnum;

import lombok.Data;

@Data
@SuppressWarnings("rawtypes")
public class ResultUtil<T> {

	private Integer code;
	
	private String msg;
	
	private T data;
	
	public ResultUtil() {
	}
	
	public ResultUtil(Integer code) {
		this.code = code;
	}
	
	public ResultUtil(Integer code, T data) {
		this.code = code;
		this.data = data;
	}

	public ResultUtil(Integer code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	
	public ResultUtil(ResultEnum resultEnum) {
		this.code = resultEnum.getCode();
		this.msg = resultEnum.getMsg();
	}
	
	/**
	 * 
	 * @Description 
	 *	成功
	 * @param data
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static ResultUtil success(Object data) {
		return new ResultUtil(0, data);
	}

	public static ResultUtil<?> success() {
		return new ResultUtil(0);
	}
	
	public static ResultUtil error(ResultEnum resultEnum) {
		return new ResultUtil(resultEnum);
	}
	
	public static ResultUtil error(String msg) {
		ResultUtil result = new ResultUtil();
		result.setCode(-1);
		result.setMsg(msg);
		return result;
	}

}
