package com.security.jwt.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description 
 *	相应信息返回前端
 * @author lt
 *
 */
@Slf4j
public class ResponseUtil {

	public static void write(HttpServletResponse response, Object object) {
		try {
			response.setContentType("application/json; charset = utf-8");
			PrintWriter out = response.getWriter();
			out.println(JSON.toJSONString(object, SerializerFeature.WriteMapNullValue));
			out.flush();
			out.close();
		} catch (IOException e) {
			log.error("e={}", e);
		}
		
	}
}
