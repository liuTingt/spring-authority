package com.security.jwt.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 
 * @Description 
 *	拦截器依赖注入bean
 * @author lt
 *
 */
@Component
public class SpringUtils implements ApplicationContextAware{
	
	private static ApplicationContext applicationContext;

	
	
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		if(SpringUtils.applicationContext == null) {
			SpringUtils.applicationContext = applicationContext;
		}
	}

	/**
	 * 
	 * @Description 
	 *	根据name
	 * @param name
	 * @return
	 */
	public static Object getBean(String name) {
		return getApplicationContext().getBean(name);
	}
	
	/**
	 * 
	 * @Description 
	 *	根据类型
	 * @param clazz
	 * @return
	 */
	public static <T> T getBean(Class<T> clazz){
		return getApplicationContext().getBean(clazz);
	}
	
	public static <T> T getBean(String name, Class<T> clazz) {
		return getApplicationContext().getBean(name, clazz);
	}

}
