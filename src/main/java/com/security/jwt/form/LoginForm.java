package com.security.jwt.form;

import lombok.Data;

/**
 * 
 * @Description 
 *
 * @author lt
 *
 */
@Data
public class LoginForm {

	private String userPhone;
	
	private String userPassword;
	
	private Boolean remember;
}
