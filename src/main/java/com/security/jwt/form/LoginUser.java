package com.security.jwt.form;

import lombok.Data;

@Data
public class LoginUser {
	
	private String userPhone;
	
	private Boolean remember;
}
