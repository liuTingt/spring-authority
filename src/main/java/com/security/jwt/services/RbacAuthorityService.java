package com.security.jwt.services;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import com.security.jwt.dto.PermissionDTO;
import com.security.jwt.dto.PermissionMap;
import com.security.jwt.mapper.PermissionMapper;

/**
 * 
 * @Description 
 *	判断是否具有权限访问当前资源
 * @author lt
 *
 */
@Component("rbacauthorityservice")
public class RbacAuthorityService {

	@Autowired
	private PermissionMapper permissionMapper;
	
	
	/**
	 * 
	 * @Description 
	 *	判断用户是否有权限
	 * @param request
	 * @param authentication
	 * @return
	 */
	public boolean hasPermission(HttpServletRequest request, Authentication authentication) {
		if(authentication.getPrincipal().equals("anonymousUser")) 
			return false;
		
		Collection<ConfigAttribute> attributes = getAttributes(request);
		// 访问资源不需要任何权限（即不需要有任何角色就可以访问，所以返回true）
		if(null == attributes || attributes.size() <= 0) 
			return true;
		// 遍历访问的url对应的权限
		for (Iterator<ConfigAttribute> iterator = attributes.iterator(); iterator.hasNext(); ) {
			ConfigAttribute attribute = iterator.next();
			// 遍历用户拥有的角色
			for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
				// 如果用户的角色在访问资源的权限中就可以访问，否则没有权限
				if(attribute.getAttribute().trim().equals(grantedAuthority.getAuthority())) {
					return true;
				}
			}
		}
		throw new AccessDeniedException("权限不足");
	}
	
	/***
	 * 
	 * @Description 
	 *	判断用户请求的URL是否在权限表中，如果在权限表中则返回所有角色，不再权限列表中则放行
	 * @param request
	 * @return
	 */
	public Collection<ConfigAttribute>  getAttributes(HttpServletRequest request){
		HashMap<String, Collection<ConfigAttribute>> map = PermissionMap.map;
		
		if(map == null) {
			map = loadResourceDefine(map);
		}
		
		for (Map.Entry<String, Collection<ConfigAttribute>> entry : map.entrySet()) {
			String url = entry.getKey();
			if(new AntPathRequestMatcher(url).matches(request)) {
				return entry.getValue();
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @Description 
	 *	加载权限表中所有权限
	 * @return
	 */
	public HashMap<String, Collection<ConfigAttribute>> loadResourceDefine(HashMap<String, Collection<ConfigAttribute>> map){
		map = new HashMap<>();
		List<PermissionDTO> all = permissionMapper.findAll();
		for (PermissionDTO permissionDTO : all) {
			List<ConfigAttribute> list = permissionDTO.getRoleNames().stream().map(roleName -> {
				ConfigAttribute attribute = new SecurityConfig("ROLE_"+roleName.toUpperCase());
				return attribute;
			}).collect(Collectors.toList());
			map.put(permissionDTO.getPermissionUrl(), list);
		}
		PermissionMap.map = map;
		return map;
	}
}
