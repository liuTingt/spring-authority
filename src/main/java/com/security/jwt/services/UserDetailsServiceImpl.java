package com.security.jwt.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.security.jwt.dto.UserDTO;
import com.security.jwt.form.LoginUser;
import com.security.jwt.mapper.UserMapper;

/**
 * 
 * @Description 
 *	
 * @author lt
 *
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public UserDetails loadUserByUsername(String json) throws UsernameNotFoundException {
		LoginUser loginUser = JSON.parseObject(json, LoginUser.class);
		
		UserDTO userDto = userMapper.getUserDTO(loginUser.getUserPhone());
		userDto.setRemember(loginUser.getRemember());
		userDto.setName(userDto.getUsername());
		userDto.setUserName(json);
		return userDto;
	}

}
