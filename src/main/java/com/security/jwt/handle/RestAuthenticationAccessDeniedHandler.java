package com.security.jwt.handle;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import com.security.jwt.enums.ResultEnum;
import com.security.jwt.util.ResponseUtil;
import com.security.jwt.util.ResultUtil;

/**
 * 
 * @Description 
 *	权限不足时调用
 * @author lt
 *
 */
public class RestAuthenticationAccessDeniedHandler implements AccessDeniedHandler{

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		ResponseUtil.write(response, ResultUtil.error(ResultEnum.ACCESS_NOT));
	}

}
