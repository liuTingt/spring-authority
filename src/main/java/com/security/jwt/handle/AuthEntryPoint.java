package com.security.jwt.handle;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.security.jwt.enums.ResultEnum;
import com.security.jwt.util.ResponseUtil;
import com.security.jwt.util.ResultUtil;

/**
 * 
 * @Description 
 *	权限认证异常处理器	
 *
 *	当用户请求没有通过认证，那么会抛出异常，AuthenticationEntryPoint.commence就会被调用，这个对应的代码在
 * 	ExceptionTranslationFilter中，当ExceptionTranslationFilter catch到异常后，就会间接调用AuthenticationEntryPoint。
 * 	在配置中指定
 * @author lt
 *
 */
public class AuthEntryPoint implements AuthenticationEntryPoint{

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException, ServletException {
		ResponseUtil.write(response, ResultUtil.error(ResultEnum.TOKEN_IS_NOT_VALID));
	}

}
