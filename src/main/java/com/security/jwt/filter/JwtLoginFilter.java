package com.security.jwt.filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.security.jwt.dto.UserDTO;
import com.security.jwt.entity.User;
import com.security.jwt.form.LoginForm;
import com.security.jwt.form.LoginUser;
import com.security.jwt.mapper.UserMapper;
import com.security.jwt.util.BlankUtil;
import com.security.jwt.util.JwtTokenUtil;
import com.security.jwt.util.ResponseUtil;
import com.security.jwt.util.ResultUtil;
import com.security.jwt.util.SpringUtils;
import com.security.jwt.util.TimeUtil;
import com.security.jwt.vo.UserVO;

/***
 * 
 * @Description 
 *	处理登录请求
 * @author lt
 *
 */
public class JwtLoginFilter extends UsernamePasswordAuthenticationFilter{

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private UserMapper userMapper;
	
	private AuthenticationManager authenticationManager;

	public JwtLoginFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}


	/***
	 * 登录请求
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		try {
			LoginForm loginForm = new ObjectMapper().readValue(request.getInputStream(), LoginForm.class);
			checkLoginForm(loginForm, response);
			LoginUser loginUser = new LoginUser();
			BeanUtils.copyProperties(loginForm, loginUser);
			
			 // authenticationManager 顾名思义就是认证管理器 , 实现类默认是ProviderManager
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(JSON.toJSONString(loginUser), loginForm.getUserPassword(), new ArrayList<>()));
		}  catch (IOException e) {
			ResponseUtil.write(response, ResultUtil.error("数据读取错误"));
		}
		return null;
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		UserDTO userDTO = (UserDTO) authResult.getPrincipal();
		if(jwtTokenUtil == null) {
			jwtTokenUtil = (JwtTokenUtil) SpringUtils.getBean("jwtTokenUtil");
		}
		
		User user = new User();
		user.setUserId(userDTO.getUserId());
		user.setUserLastLoginTime(TimeUtil.nowTimeStamp());
		if(userMapper == null) {
			userMapper = (UserMapper) SpringUtils.getBean("userMapper");
		}
		// 更新最近一次登录时间
		userMapper.updateById(user);
		String token = jwtTokenUtil.generationToken(userDTO);
		// 将token放到请求头
		response.addHeader(jwtTokenUtil.getTokenHeader(), jwtTokenUtil.getTokenPrefix() + token);
		UserVO userVO = new UserVO();
		BeanUtils.copyProperties(userDTO, userVO);
		ResponseUtil.write(response, ResultUtil.success(userVO));
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		ResponseUtil.write(response, ResultUtil.error(failed.getMessage()));
	}
	
	private void checkLoginForm(LoginForm loginform, HttpServletResponse response) {
		if(BlankUtil.isBlank(loginform.getUserPhone())) {
			ResponseUtil.write(response, "手机号不能为空");
		}
		if(BlankUtil.isBlank(loginform.getUserPassword())) {
			ResponseUtil.write(response, "密码不能为空");
		}
	}
	
}
