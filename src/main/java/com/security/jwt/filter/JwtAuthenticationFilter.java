package com.security.jwt.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.security.jwt.dto.UserDTO;
import com.security.jwt.util.JwtTokenUtil;
import com.security.jwt.util.SpringUtils;

/**
 * 
 * @Description 
 *	校验过滤器
 * @author lt
 *
 */
public class JwtAuthenticationFilter extends BasicAuthenticationFilter{

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// 验证token是否有效
		if(jwtTokenUtil == null) {
			jwtTokenUtil = (JwtTokenUtil) SpringUtils.getBean("jwtTokenUtil");
		}
		
		String header = request.getHeader(jwtTokenUtil.getTokenHeader());
		// 当token为空或者格式错误的时候直接放行
		if(header == null || !header.startsWith(jwtTokenUtil.getTokenPrefix())) {
			chain.doFilter(request, response);
			return;
		}
		
		UsernamePasswordAuthenticationToken authentication = getAuthentication(header);
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(request, response);
	}
	
	/***
	 * 
	 * @Description 
	 *	从token中获取用户信息并建立一个token
	 * @param header
	 * @return
	 */
	private UsernamePasswordAuthenticationToken getAuthentication(String header) {
		String token = header.replace(jwtTokenUtil.getTokenPrefix(), "");
		String principal = jwtTokenUtil.getUserNameFromToken(token);
		if(principal != null) {
			UserDTO userDTO = jwtTokenUtil.getUserDTO(token);
			return new UsernamePasswordAuthenticationToken(principal, null, userDTO.getAuthorities());
		}
		return null;
	}

}
