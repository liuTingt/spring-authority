package com.security.jwt.vo;

import java.util.List;

import lombok.Data;

@Data
public class UserVO {
	/**
	 * 编号
	 */
	private String userId;
	
	/**
	 * 用户名
	 */
	private  String name;
	
	/**
	 * 手机号
	 */
	private String userPhone;
	
	/**
	 * 角色
	 */
	private List<String> roles;
}
