package com.security.jwt.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.security.jwt.filter.JwtAuthenticationFilter;
import com.security.jwt.filter.JwtLoginFilter;
import com.security.jwt.handle.AuthEntryPoint;
import com.security.jwt.handle.RestAuthenticationAccessDeniedHandler;
import com.security.jwt.services.UserDetailsServiceImpl;

/**
 * 
 * @Description 
 *
 * @author lt
 *
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter{

	@Autowired
	private UserDetailsServiceImpl userDetailsService;
	
	/**
	 * 
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(new BCryptPasswordEncoder());
	}


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().authorizeRequests()
			.antMatchers("/user/hello").permitAll()
			.antMatchers("/user/logout").permitAll()
			//.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // 使用 JWT，关闭token
			.anyRequest().authenticated()
			.anyRequest().access("@rbacauthorityservice.hasPermission(request,authentication)")// RBAC 动态 url 认证, 持有权限的都可以访问
			.and()
			.addFilter(new JwtLoginFilter(authenticationManager()))
			.addFilter(new JwtAuthenticationFilter(authenticationManager()))
			.exceptionHandling().accessDeniedHandler(new RestAuthenticationAccessDeniedHandler())
			.authenticationEntryPoint(new AuthEntryPoint());
	}

	
	
}
