package com.security.jwt.dto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.security.jwt.vo.UserVO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UserDTO extends UserVO implements UserDetails{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 密码
	 */
	private String userPassword;
	
	/**
	 * 是否记住密码
	 */
	private Boolean remember;
	
	/**
	 * 权限
	 */
	//private  Set<GrantedAuthority> authorities;
	
	/**
	 * 账号是否未过期
	 */
	private  boolean accountNonExpired = true;
	
	/**
	 * 账户是否未锁定
	 */
	private  boolean accountNonLocked = true;
	
	/**
	 * 凭证是否未过期
	 */
	private  boolean credentialsNonExpired = true;
	
	/**
	 * 
	 */
	private  boolean enabled = true;
	
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<GrantedAuthority> list = getRoles().stream()
				.map(roleName -> new SimpleGrantedAuthority("ROLE_" + roleName))
				.collect(Collectors.toList());
		return list;
	}

	@Override
	public String getPassword() {
		return userPassword;
	}

	@Override
	public String getUsername() {
		return userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

}
