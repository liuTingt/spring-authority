package com.security.jwt.dto;

import java.util.List;

import lombok.Data;

/**
 * 
 * @Description
 *	角色资源
 * @author lt
 *
 */
@Data
public class PermissionDTO {

	/**
	 * url
	 */
	private String permissionUrl;
	
	/**
	 * 角色名称
	 */
	private List<String> roleNames; 
}
