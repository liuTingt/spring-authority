package com.security.jwt.dto;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.security.access.ConfigAttribute;

import lombok.Data;
/**
 * 
 * @Description 
 *	权限相关
 * @author lt
 *
 */
@Data
public class PermissionMap {

	public static HashMap<String, Collection<ConfigAttribute>> map;
	
	public static List<PermissionDTO> list;
}
